﻿using UnityEngine;
using FRI.Movement;
using FRI.Core;

namespace FRI.Control
{
    [RequireComponent(typeof(Mover))]
    [RequireComponent(typeof(Jetpack))]
    public class PlayerController : MonoBehaviour
    {
        [Range(0f, 1f)]
        [SerializeField] private float jetpackAxisThreshold = 0.7f; // controller button press threshold

        private Mover mover = null;
        private Jetpack jetpack = null;
        private GamePauser pauser = null;

        private void Awake() 
        {
            mover = GetComponent<Mover>();
            jetpack = GetComponent<Jetpack>();
            pauser = FindObjectOfType<GamePauser>();
        }

        private void Update()
        {
            if (GameManager.IsGameOver) return;

            if (Input.GetButtonDown("Start")) // open pause menu
                pauser.TogglePause();

            if (GamePauser.IsPaused) return;

            mover.HorizontalInput = Input.GetAxisRaw("Horizontal");

            if (Input.GetButtonDown("Jump"))
                mover.JumpInput = true;

            var jetpackAxisInput = Input.GetAxisRaw("Jetpack");
            if (jetpackAxisInput >= jetpackAxisThreshold || Input.GetButtonDown("Jetpack")
             && !jetpack.Active)
            {
                jetpack.Active = true;
            }
            else if (!Input.GetButton("Jetpack") && jetpackAxisInput < jetpackAxisThreshold && jetpack.Active)
            {
                jetpack.Active = false;
            }
        }
    }   
}
