﻿using System.Collections;
using FRI.Movement;
using UnityEngine;

namespace FRI.Control
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private bool moveRight = true;
        [Tooltip("Specifies how long an enemy should wait at the edge of a platform.")]
        [SerializeField] private float lingerLength = 2f;
        [Space]
        [Tooltip("Specifies how deep the enemy should look for platform under itself.")]
        [SerializeField] private float platformCheckRayLength = 0.2f;
        [Tooltip("Specifies how far from itself enemy should look for platform edge.")]
        [SerializeField] private float platformCheckHorizontalOffset = 0f;

        private Mover mover = null;
        private Rigidbody2D rb = null;

        private bool lingering = false;
        // Used for platform edge checking
        private Vector2 rightBottom = Vector2.zero;
        private Vector2 leftBottom = Vector2.zero;
        private bool wasAtRightEdge = false;
        private bool wasAtLeftEdge = false;

        private void Awake() 
        {
            mover = GetComponent<Mover>();
            rb = GetComponent<Rigidbody2D>();
        }

        private void Update() 
        {
            if (lingering) return;

            mover.HorizontalInput = moveRight ? 1f : -1f;

            CheckForPlatformEdge();
        }

        private void CheckForPlatformEdge()
        {
            GetBottomCorners();
            if (!wasAtRightEdge)
            {
                wasAtRightEdge = CheckForEdge(rightBottom);
                if (wasAtRightEdge)
                {
                    wasAtLeftEdge = false;
                    return;
                }
            }
            
            if (!wasAtLeftEdge)
            {
                wasAtLeftEdge = CheckForEdge(leftBottom);
                if (wasAtLeftEdge)
                    wasAtRightEdge = false;
            }
        }

        /// <summary>
        /// Updates position of platform edge check points based on enemy's current position.
        /// </summary>
        private void GetBottomCorners()
        {
            var sr = GetComponent<Collider2D>();

            float halfHeight = sr.bounds.extents.y;
            float halfWidth = sr.bounds.extents.x;

            Vector2 position = transform.position;
            rightBottom = new Vector2(position.x + halfWidth + platformCheckHorizontalOffset, position.y - halfHeight);
            leftBottom = new Vector2(position.x - halfWidth - platformCheckHorizontalOffset, position.y - halfHeight);
        }

        /// <summary>
        /// Checks if enemy reached the edge of platform.
        /// </summary>
        /// <param name="checkPosition">Where the edge should be searched for.</param>
        /// <returns>True if no platform is present at checkPosition.</returns>
        private bool CheckForEdge(Vector2 checkPosition)
        {
            RaycastHit2D hit;
            hit = Physics2D.Raycast(checkPosition, Vector2.down, platformCheckRayLength, LayerMask.GetMask("Ground"));
            if (hit.collider == null)
            {
                StartCoroutine(Linger());
                return true;
            }
            return false;
        }

        /// <summary>
        /// Makes enemy wait for lingerLength seconds at the edge of a platform.
        /// </summary>
        private IEnumerator Linger()
        {
            mover.HorizontalInput = 0f;
            lingering = true;
            yield return new WaitForSeconds(lingerLength);
            lingering = false;
            TurnAround();
        }

        private void TurnAround()
        {
            moveRight = !moveRight;
        }

        private void OnCollisionEnter2D(Collision2D other) 
        {
            if (other.gameObject.tag != "Player") return;

            rb.constraints = RigidbodyConstraints2D.FreezeAll; // prevents player from knocking enemy away
        }

        private void OnCollisionExit2D(Collision2D other)
        {
            if (other.gameObject.tag != "Player") return;
            
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        }

        /// <summary>
        /// Draws helper gizmos in editor. These indicate where the enemy checks for the edge of platform.
        /// </summary>
        private void OnDrawGizmosSelected()
        {
            if (rightBottom == Vector2.zero)
                GetBottomCorners();

            Gizmos.color = Color.black;
            Gizmos.DrawLine(rightBottom, new Vector2(rightBottom.x, rightBottom.y - platformCheckRayLength));
            Gizmos.DrawLine(leftBottom, new Vector2(leftBottom.x, leftBottom.y - platformCheckRayLength));
        }
    }   
}
