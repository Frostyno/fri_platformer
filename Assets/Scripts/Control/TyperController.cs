﻿using System.Collections;
using System.Collections.Generic;
using FRI.Core;
using UnityEngine;

namespace FRI.Control
{
    [RequireComponent(typeof(ScoreHolder))]
    public class TyperController : MonoBehaviour
    {
        private TyperManager tm = null;

        private ScoreHolder sh = null;
        private GamePauser pauser = null;

        private bool wasPausedThisFrame = false;

        private void Awake()
        {
            sh = GetComponent<ScoreHolder>();
            tm = FindObjectOfType<TyperManager>();
            tm.OnWordWritten += OnWordWritten;
            pauser = FindObjectOfType<GamePauser>();
            pauser.OnUnpause += OnUnpause;
        }

        void Update()
        {
            if (GameManager.IsGameOver) return;

            if (Input.GetButtonDown("Start")) // open pause menu
                pauser.TogglePause();

            if (GamePauser.IsPaused) return;

            if (wasPausedThisFrame) // prevent input from unpausing the game
            {
                wasPausedThisFrame = false;
                return;
            }

            if (Input.GetKeyDown(KeyCode.Backspace))
            {
                tm.RemoveLastInputCharacter();
                return;
            }

            if (Input.GetKeyDown(KeyCode.Return))
            {
                tm.ValidateInput();
                return;
            }

            if (Input.anyKeyDown)
            {
                tm.AddInputCharacter(Input.inputString);
            }
        }

        private void OnWordWritten(WordWrittenEventArgs args)
        {
            sh.AddScore(ScoreType.Word, args.scoreGained);
        }

        private void OnUnpause()
        {
            wasPausedThisFrame = true;
        }
    }   
}
