﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace FRI.SceneManagement
{
    public class SceneLoader : MonoBehaviour
    {
        [SerializeField] private int sceneBuildIndex = -1;

        public void LoadSceneAsync()
        {
            StartCoroutine(LoadScene());
        }

        private IEnumerator LoadScene()
        {
            yield return SceneManager.LoadSceneAsync(sceneBuildIndex);
        }
    }
}
