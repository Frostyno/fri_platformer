﻿namespace FRI.PickUps
{
    /// <summary>
    /// Interface for defining individual pick up effects used by Pickupable.
    /// </summary>
    public interface IPickUpEffect
    {
        void ApplyEffect();
    }
}
