﻿using FRI.Core;
using FRI.Movement;
using UnityEngine;

namespace FRI.PickUps
{
    public class FuelPickUpEffect : MonoBehaviour, IPickUpEffect
    {
        [SerializeField] private float fuelToGain = 20;

        public void ApplyEffect()
        {
            Jetpack jetpack = GameManager.Instance.Player.GetComponent<Jetpack>();
            jetpack.GainFuel(fuelToGain);
        }
    }
}
