﻿using UnityEngine;

namespace FRI.PickUps
{
    [RequireComponent(typeof(Collider2D))]
    public class Pickupable : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.tag != "Player") return;

            IPickUpEffect[] pickUpEffects = GetComponents<IPickUpEffect>(); // get all pick up effects attached to this GameObject

            foreach (var pickUp in pickUpEffects) // apply effects
            {
                pickUp.ApplyEffect();
            }

            Destroy(gameObject);
        }
    }  
}

