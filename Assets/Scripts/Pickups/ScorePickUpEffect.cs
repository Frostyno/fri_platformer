﻿using FRI.Core;
using UnityEngine;

namespace FRI.PickUps
{
    [RequireComponent(typeof(Pickupable))]
    public class ScorePickUpEffect : MonoBehaviour, IPickUpEffect
    {
        [SerializeField] private int scoreToAdd = 10;

        public void ApplyEffect()
        {
            ScoreHolder scoreHolder = GameManager.Instance.Player.GetComponent<ScoreHolder>();
            scoreHolder.AddScore(ScoreType.PickUp, scoreToAdd);
        }
    }
}

