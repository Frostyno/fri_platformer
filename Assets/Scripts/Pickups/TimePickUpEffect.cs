﻿using FRI.Core;
using UnityEngine;

namespace FRI.PickUps
{
    public class TimePickUpEffect : MonoBehaviour, IPickUpEffect
    {
        [SerializeField] private float timeToAdd = 5f;

        public void ApplyEffect()
        {
            CountdownTimeHolder timeHolder = GameManager.Instance.Player.GetComponent<CountdownTimeHolder>();
            timeHolder.AddTime(timeToAdd);
        }
    }
}
