﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRI.Core
{
    /// <summary>
    /// Spawns persistent object at the first time the GameObject this script is attached to
    /// comes awake.
    /// Persistent object is not destroyed on scene load.
    /// </summary>
    public class PersistentObjectSpawner : MonoBehaviour
    {
        [SerializeField] private GameObject persistentObjectPrefab = null;

        private static bool hasSpawned = false;

        private void Awake()
        {
            if (hasSpawned) return;

            if (persistentObjectPrefab == null)
                Debug.LogError("Persistent object prefab is missing in " + name);

            SpawnPersistentObject();

            hasSpawned = true;
        }

        private void SpawnPersistentObject()
        {
            GameObject persistentObject = Instantiate(persistentObjectPrefab);
            DontDestroyOnLoad(persistentObject);
        }
    }
}
