﻿using System;
using UnityEngine;

namespace FRI.Core
{
    /// <summary>
    /// Holds game time left for player.
    /// When time runs out player loses.
    /// </summary>
    public class CountdownTimeHolder : MonoBehaviour
    {
        public Action<float> OnCountdownTimeChanged = null;

        [SerializeField] private float countdownTimeLeft = 10f;
        public float CountdownTimeLeft { get => countdownTimeLeft; }

        private bool timeRunOut = false;
        public bool TimeRunOut { get => timeRunOut; }

        private void Start()
        {
            var gm = GameManager.Instance;
            if (gm.Config != null)
            {
                countdownTimeLeft = gm.Config.gameLength;
            }
        }

        public void AddTime(float ammount)
        {
            countdownTimeLeft += ammount;
            OnCountdownTimeChanged?.Invoke(countdownTimeLeft);
        }

        private void Update() 
        {
            if (timeRunOut) return;

            countdownTimeLeft -= Time.deltaTime;
            OnCountdownTimeChanged?.Invoke(countdownTimeLeft);

            if (countdownTimeLeft <= 0f)
            {
                timeRunOut = true;
                GameManager.Instance.GameOver();
            }
        }
    }
}
