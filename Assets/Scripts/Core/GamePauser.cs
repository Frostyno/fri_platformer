﻿using System;
using UnityEngine;

namespace FRI.Core
{
    /// <summary>
    /// Handles pausing/unpausing of game's cycle.
    /// </summary>
    public class GamePauser : MonoBehaviour
    {
        private static bool _isPaused = false;
        public static bool IsPaused { get => _isPaused; private set => _isPaused = value; }

        public Action OnPause = null;
        public Action OnUnpause = null;

        [SerializeField] private GameObject menuPanel = null;
        [SerializeField] private GameObject inGameUI = null;

        private void Awake() 
        {
            IsPaused = false;
            Time.timeScale = 1f;
        }

        public void TogglePause()
        {
            if (IsPaused)
                UnpauseGame();
            else
                PauseGame();
        }

        private void PauseGame()
        {
            Time.timeScale = 0f; // stop game's update loop
            menuPanel.SetActive(true);
            if (inGameUI != null) inGameUI.SetActive(false);
            IsPaused = true;
            OnPause?.Invoke();
        }

        private void UnpauseGame()
        {
            menuPanel.SetActive(false);
            if (inGameUI != null) inGameUI.SetActive(true);
            Time.timeScale = 1f; // resume game's update loop
            IsPaused = false;
            OnUnpause?.Invoke();
        }
        
        /// <summary>
        /// Called when object is destroyed, e.g. loading a new scene.
        /// </summary>
        private void OnDestroy() 
        {
            IsPaused = false;
            Time.timeScale = 1f;
        }
    }
}
