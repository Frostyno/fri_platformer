﻿[System.Serializable]
public class Configuration
{
    public int gameLength = 40;
    public string serverURL = "http://sharp.kst.fri.uniza.sk/UnityFriGames:80";
    public bool gamepadMode = true;
}
