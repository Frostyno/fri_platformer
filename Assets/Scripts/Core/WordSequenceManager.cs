﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using FRI.Saving;
using UnityEngine;
using Proyecto26;
using FRI.Models;
using System.Globalization;

namespace FRI.Core
{
    public class WordSequenceManager : MonoBehaviour
    {
        private string serverURL = "http://localhost:5000/api/highscores/typer/sequence";

        private void Start()
        {
            serverURL = GameManager.Instance.Config.serverURL + "/typer/sequence";
        }

        /// <summary>
        /// Adds new sequence and related information to the server's database.
        /// </summary>
        /// <param name="word">Word that player was trying to type.</param>
        /// <param name="sequence">Player's input sequence.</param>
        /// <param name="timeTaken">Time it took player to write input sequence.</param>
        public void AddNewSequence(string word, string sequence, float timeTaken)
        {
            // AddNewSequence(new WordSequence(word, sequence, timeTaken));

            string body = '{' + string.Format("\"word\":\"{0}\",\"inputsequence\":\"{1}\",\"timetaken\":{2}", word, sequence, timeTaken.ToString(CultureInfo.InvariantCulture)) + '}';

            Debug.Log(serverURL);
            Debug.Log(body);

            RestClient.Post<SequenceModel>(serverURL, body)
            .Then(res =>
            {
                Debug.Log("to");
#if UNITY_EDITOR
                Debug.Log(string.Format("ID: {0}, word: {1}, is: {2}, timetaken: {3}", res.id, res.word, res.inputsequence, res.timetaken));
#endif
            });
        }
    }   
}
