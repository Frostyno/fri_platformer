﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRI.Core
{
    public class FollowTarget : MonoBehaviour
    {
        [SerializeField] private Transform followTarget = null;
        [SerializeField] private float yOffset = 0f;

        /// <summary>
        /// Updates position off GameObject it is attached to based on followTarget's position.
        /// </summary>
        void LateUpdate()
        {
            Vector3 newPosition = transform.position;
            newPosition.y = followTarget.position.y + yOffset;
            transform.position = newPosition;
        }
    }
}
