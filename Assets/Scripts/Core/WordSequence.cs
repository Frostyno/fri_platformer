﻿namespace FRI.Core
{
    [System.Serializable]
    public class WordSequence
    {
        public string word;
        public string inputSequence;
        public float timeTaken;

        public WordSequence(string w, string iSeq, float tt)
        {
            word = w;
            inputSequence = iSeq;
            timeTaken = tt;
        }
    }    
}
