﻿using FRI.Saving;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using UnityEngine;
using Proyecto26;
using FRI.Models;

namespace FRI.Core
{
    /// <summary>
    /// Deprecated.
    /// </summary>
    public class HighscoreManager : MonoBehaviour
    {
        private void Start()
        {
            GetServerAuthorization();
        }

        private void GetServerAuthorization()
        {
            string url = GameManager.Instance.Config.serverURL + "/authenticate";
            string body = "{\"name\": \"Frickovica\",\"password\": \"frijenaj2020\"}";
            
            RestClient.Post<User>(url, body).Then
            (
                response =>
                {
                    RestClient.DefaultRequestHeaders["Authorization"] = "Bearer " + response.token;
                }
            );
        }
        // [SerializeField] private string FILE_NAME = "highscores.json";

        // private string c_highscoresFilePath = null;

        // private List<HighscoreItem> highscores = null;
        // public ReadOnlyCollection<HighscoreItem> Highscores { get; private set; }

        // private void Awake()
        // {
        //     c_highscoresFilePath = Path.Combine(Application.persistentDataPath, FILE_NAME);

        //     highscores = JsonFileHandler.Deserialize<List<HighscoreItem>>(c_highscoresFilePath);
        //     if (highscores == null)
        //         highscores = new List<HighscoreItem>();

        //     Highscores = new ReadOnlyCollection<HighscoreItem>(highscores);
        // }

        // public void AddScore(int score, string name)
        // {
        //     AddScore(new HighscoreItem(score, name));
        // }

        // public void AddScore(HighscoreItem newItem)
        // {
        //     int index = 0;
        //     foreach (var item in highscores)
        //     {
        //         if (item.score <= newItem.score) break;
        //         index++;
        //     }
        //     highscores.Insert(index, newItem);
        // }

        // private void OnDestroy()
        // {
        //     // NOTE: this script is present on persistent object, e.g. isn't destroyed on scene load
        //     if (highscores.Count == 0) return;

        //     highscores.Sort();
        //     JsonFileHandler.Serialize(highscores, c_highscoresFilePath);
        // }
    }
}
