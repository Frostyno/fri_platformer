﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRI.Core
{
    /// <summary>
    /// Watches what height the player reached.
    /// Adds various bonuses when appropriate.
    /// </summary>
    [RequireComponent(typeof(ScoreHolder))]
    [RequireComponent(typeof(CountdownTimeHolder))]
    public class HeightWatcher : MonoBehaviour
    {
        [Tooltip("Specifies how often score reward should be given.")]
        [SerializeField] private float scoreHeightStep = 10f;
        [Tooltip("Specifies how big the reward for new height record is.")]
        [SerializeField] private int scoreStepReward = 10;
        [Space]
        [SerializeField] private float timeHeightStep = 10f;
        [SerializeField] private float timeStepReward = 2f;

        private ScoreHolder scoreHolder = null;
        private CountdownTimeHolder timeHolder = null;

        private float highestHeight = 0f;
        private float lastGivenScoreHeight = 0f;
        private float lastGivenTimeHeight = 0f;

        private void Awake() 
        {
            scoreHolder = GetComponent<ScoreHolder>();
            timeHolder = GetComponent<CountdownTimeHolder>();
        }

        private void Update()
        {
            CheckHeight();
        }

        private void CheckHeight()
        {
            // only execute if player is higher than ever before
            if (transform.position.y > highestHeight)
            {
                highestHeight = transform.position.y;

                // Add score if player reached new score height step
                if (highestHeight >= lastGivenScoreHeight + scoreHeightStep)
                {
                    lastGivenScoreHeight += scoreHeightStep;
                    scoreHolder.AddScore(ScoreType.Height, scoreStepReward);
                }
                // Add time if player reached new time height step
                if (highestHeight >= lastGivenTimeHeight + timeHeightStep)
                {
                    lastGivenTimeHeight += timeHeightStep;
                    timeHolder.AddTime(timeStepReward);
                }
            }
        }
    }
}
