﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace FRI.Core
{
    /// <summary>
    /// Holds all score earned by player.
    /// Score is sorted based on source.
    /// </summary>
    public class ScoreHolder : MonoBehaviour
    {
        private Dictionary<ScoreType, int> scoresByType = new Dictionary<ScoreType, int>();
        private int totalScore = 0;

        public ReadOnlyDictionary<ScoreType, int> ScoresByType;
        public int TotalScore { get => totalScore; }

        public Action<int> OnTotalScoreChanged = null;

        public ScoreHolder()
        {
            ScoresByType = new ReadOnlyDictionary<ScoreType, int>(scoresByType);
        }

        public void AddScore(ScoreType type, int ammount)
        {
            if (!scoresByType.ContainsKey(type))
                scoresByType[type] = 0;

            scoresByType[type] += ammount;
            totalScore += ammount;

            OnTotalScoreChanged?.Invoke(totalScore);
        }

        public int GetTotalScore()
        {
            return totalScore;
        }

        public void Reset()
        {
            totalScore = 0;
            scoresByType.Clear();
        }
    }
}
