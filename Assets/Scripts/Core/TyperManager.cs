﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using FRI.Generation;
using UnityEngine;
using System.Text;

namespace FRI.Core
{
    #region EventArgs
    public struct WordGeneratedEventArgs
    {
        public Vector2 position;
        public string generatedWord;

        public WordGeneratedEventArgs(Vector2 pos, string word)
        {
            position = pos;
            generatedWord = word;
        }
    }

    public struct WordWrittenEventArgs
    {
        public Vector2 position;
        public int scoreGained;
        public float timeTaken;

        public WordWrittenEventArgs(Vector2 pos, int score, float time)
        {
            position = pos;
            scoreGained = score;
            timeTaken = time;
        }
    }

    public struct CharacterValidationEventArgs
    {
        public int index;
        public bool correct;
        public char input;

        public CharacterValidationEventArgs(int i, bool c, char inp)
        {
            index = i;
            correct = c;
            input = inp;
        }
    }
    #endregion

    /// <summary>
    /// Manages word generation, giving out score for correct words written and similar.
    /// </summary>
    public class TyperManager : MonoBehaviour
    {
        public Action<WordWrittenEventArgs> OnWordWritten = null;
        public Action<WordGeneratedEventArgs> OnWordGenerated = null;
        public Action<CharacterValidationEventArgs> OnCharacterValidation = null;
        public Action<int> OnCharacterRemoval = null;

        private LinkedList<Vector2> platformPositions = new LinkedList<Vector2>();

        [Tooltip("Specifies how much score should a player be given when he writes a character correctly")]
        [SerializeField] private int scorePerCorrectCharacter = 10;
        [SerializeField] private int scorePerIncorrectCharacter = -5;

        private bool wordActive = false; // specifies whether there is a word displayed on screen
        private Vector2 currentPosition = Vector2.zero; // position of platform where currently displayed word is
        private string currentWord = null;
        private StringBuilder playerInput = null;
        private int correctCharacters = 0;
        private int incorrectCharacters = 0;
        private float timeSinceWordGeneration = 0f;

        private string[] words = null; // all words that can be generated
        private System.Random wordChoiceGenerator = null;

        private void Start()
        {
            playerInput = new StringBuilder();

            GetComponent<PlatformGenerator>().OnPlatformSpawned += OnPlatformSpawned;

            wordChoiceGenerator = new System.Random(GameManager.Instance.SeedGenerator.Next());

            try
            {
                // gets all words that can be generated in game
                words = File.ReadAllLines(Path.Combine(Application.streamingAssetsPath, "typer.txt"));
            }
            catch (Exception e)
            {
                Debug.LogError("Cannot read words file. Exception " + e);
            }
        }

        private void Update()
        {
            if (wordActive)
            {
                timeSinceWordGeneration += Time.deltaTime;
            }
        }

        /// <summary>
        /// Validates inputed string based on currently generated word.
        /// Generates a new word.
        /// </summary>
        public void ValidateInput()
        {
            int score = correctCharacters * scorePerCorrectCharacter;
            score += incorrectCharacters * scorePerIncorrectCharacter;

            OnWordWritten?.Invoke(new WordWrittenEventArgs(currentPosition, score, timeSinceWordGeneration));

            GenerateNewWord();
        }

        /// <summary>
        /// Removes last inputed character.
        /// </summary>
        public void RemoveLastInputCharacter()
        {
            if (playerInput.Length == 0) return;

            // check if removed character was correct
            bool correct = playerInput.ToString()[playerInput.Length - 1] == currentWord[playerInput.Length - 1];

            if (correct) correctCharacters--;
            incorrectCharacters++; // not inputed character counts as incorrect

            playerInput.Remove(playerInput.Length - 1, 1);
            OnCharacterRemoval?.Invoke(playerInput.Length);
        }

        public void AddInputCharacter(string input)
        {
            if (input.Length != 1) return; // only enable inputing single characters
            if (playerInput.Length >= currentWord.Length) return; // ignore characters that would exceed generated word's length

            playerInput.Append(input);

            bool correct = input[0] == currentWord[playerInput.Length - 1]; // check if inputed character was correct

            if (correct) 
            {
                correctCharacters++;
                incorrectCharacters--;
            }

            OnCharacterValidation?.Invoke(new CharacterValidationEventArgs(playerInput.Length - 1, correct, input[0]));
        }

        private void OnPlatformSpawned(Vector2 position)
        {
            platformPositions.AddLast(position); // remember platform positions (used to place word display and player after validating word)

            if (wordActive == false) GenerateNewWord();
        }

        private void GenerateNewWord()
        {
            if (GameManager.IsGameOver) return;
            if (platformPositions.Count == 0) // words are only generated for platforms
            {
                Debug.LogError("Cannot generate new word. No platform positions available.");
                return;
            }

            playerInput.Clear();
            correctCharacters = 0;
            timeSinceWordGeneration = 0f;

            // generate new word
            int wordIndex = wordChoiceGenerator.Next() % words.Length;
            currentWord = words[wordIndex];

            // get targeted platform's position
            currentPosition = platformPositions.First.Value;
            platformPositions.RemoveFirst();

            wordActive = true;
            incorrectCharacters = currentWord.Length; // with no player input all characters are considered incorrect

            OnWordGenerated?.Invoke(new WordGeneratedEventArgs(currentPosition, currentWord));
        }
    }
}
