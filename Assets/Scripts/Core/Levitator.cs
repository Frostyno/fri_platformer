﻿using UnityEngine;

/// <summary>
/// Makes the object it is attached to levitate and rotate.
/// </summary>
public class Levitator : MonoBehaviour
{
    [SerializeField] private float degreesPerSecond = 15f;
    [SerializeField] private float range = 0.5f;
    [SerializeField] private float speed = 1f;

    public float Range { get => range; }

    private Vector3 initialPosition = Vector3.zero;

    private void Awake() 
    {
        initialPosition = transform.position;
    }

    public void SetInitialPosition(Vector3 initPosition)
    {
        transform.position = initPosition;
        initialPosition = initPosition;
    }

    private void FixedUpdate() 
    {
        transform.Rotate(0f, 0f, Time.fixedDeltaTime * degreesPerSecond);

        var temp = initialPosition;
        temp.y += Mathf.Sin(Time.fixedTime * Mathf.PI * speed) * range;
        transform.position = temp;
    }
}
