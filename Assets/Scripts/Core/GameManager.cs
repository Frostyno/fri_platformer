﻿using System;
using System.IO;
using FRI.Saving;
using UnityEngine;
using FRI.Models;
using Proyecto26;

namespace FRI.Core
{
    public class GameManager : MonoBehaviour
    {
        private static GameManager instance = null;
        public static GameManager Instance { get => instance; }

        private static bool _isGameOver = false;
        public static bool IsGameOver { get => _isGameOver; private set => _isGameOver = value; }

        [SerializeField] private bool useSeed = false;
        [Tooltip("Seed for gamewide seed generator, used to get seeds for other random generators.")]
        [SerializeField] private int seed = 0;

        public Action OnGameOver = null;

        private System.Random seedGenerator = null;
        public System.Random SeedGenerator { get => seedGenerator; }

        private GameObject player = null;
        public GameObject Player { get => player; }

        public Configuration Config { get; private set; }

        private void Awake()
        {
            // Singleton pattern, if instance exists destroy this game object
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }

            player = GameObject.FindGameObjectWithTag("Player");
            
            if (useSeed)
                seedGenerator = new System.Random(seed);
            else
                seedGenerator = new System.Random();

            Config = JsonFileHandler.Deserialize<Configuration>(Path.Combine(Application.streamingAssetsPath, "config.json"));
        }

        public void GameOver()
        {
            IsGameOver = true;
            OnGameOver?.Invoke();
        }

        private void OnDestroy()
        {
            IsGameOver = false;
        }
    }
}
