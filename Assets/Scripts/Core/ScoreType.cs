﻿namespace FRI.Core
{
    public enum ScoreType
    {
        Height = 0,
        PickUp = 1,
        Word = 2
    }
}