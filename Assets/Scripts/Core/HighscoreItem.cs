﻿using System;

namespace FRI.Core
{
    [System.Serializable]
    public class HighscoreItem : IComparable<HighscoreItem>
    {
        public string name = null;
        public int score = 0;

        public HighscoreItem(int score, string name)
        {
            this.score = score;
            this.name = name;
        }

        public int CompareTo(HighscoreItem other)
        {
            if (other == null) return -1;
            if (score > other.score) return -1;
            if (score == other.score) return 0;
            return 1;
        }
    }
}
