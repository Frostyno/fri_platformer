﻿using System;
using System.Collections.Generic;
using FRI.Core;
using UnityEngine;

namespace FRI.Generation
{
    public class EnemyGenerator : MonoBehaviour
    {
        [SerializeField] private GameObject[] enemyPrefabs = null; // set in editor
        [SerializeField] private Transform enemyParent = null; // can be null (enemies wont be parented in hierarchy)

        [Space]

        [Range(0f, 1f)]
        [SerializeField] private float spawnChance = 0.5f;

        private System.Random prefabChoiceGenerator = null;
        private System.Random spawnProbabilityGenerator = null;

        private List<GameObject> enemies = new List<GameObject>();

        private void Start() 
        {
            var gm = FindObjectOfType<GameManager>();
            prefabChoiceGenerator = new System.Random(gm.SeedGenerator.Next());
            spawnProbabilityGenerator = new System.Random(gm.SeedGenerator.Next());

            GetComponent<PlatformGenerator>().OnPlatformSpawned += OnPlatformSpawned;

            if (enemyPrefabs.Length == 0)
                Debug.LogError("No enemy prefabs set in " + name);
        }

        public bool DeleteEnemy(GameObject enemy)
        {
            if (enemies.Count == 0) return false;

            return enemies.Remove(enemy);
        }

        private void OnPlatformSpawned(Vector2 surfacePosition)
        {
            SpawnEnemy(surfacePosition);
        }

        /// <summary>
        /// Spawns new enemy on platform.
        /// </summary>
        /// <param name="surfacePosition">Position of platform's surface.</param>
        private void SpawnEnemy(Vector2 surfacePosition)
        {
            if (enemyPrefabs.Length == 0) return;
            if (!ShouldSpawnEnemy()) return; // only spawn enemy within probability

            // Create enemy
            int prefabChoice = prefabChoiceGenerator.Next(enemyPrefabs.Length);
            GameObject enemy = Instantiate(enemyPrefabs[prefabChoice], enemyParent);
            enemies.Add(enemy);

            // Set enemy's position
            SpriteRenderer renderer = enemy.GetComponent<SpriteRenderer>();
            enemy.transform.position = new Vector2(surfacePosition.x, surfacePosition.y + renderer.bounds.extents.y);
        }

        private bool ShouldSpawnEnemy()
        {
            double probability = spawnProbabilityGenerator.NextDouble();
            return probability < spawnChance;
        }
    }
}
