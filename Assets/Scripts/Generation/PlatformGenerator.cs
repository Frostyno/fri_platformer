﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using FRI.Core;
using UnityEngine;

namespace FRI.Generation
{
    public class PlatformGenerator : MonoBehaviour
    {
        [SerializeField] private GameObject[] platformPrefabs = null; // set in editor
        [SerializeField] private Transform platformParent = null; // can be null

        [Space]

        // defines zone in which platforms can be spawned (world's extends)
        [SerializeField] private float worldHalfWidth = 5f;

        [Space]
        [Tooltip("Specifies how many platforms should be spawn at the beinning of a game.")]
        [SerializeField] private int initialPlatformCount = 5;
        [SerializeField] private float generationThreshhold = 2f; // distance between player and highest platform, triggers new platform spawn

        [Space]

        // platform settings
        [SerializeField] private float currentMinWidth = 3f;
        [SerializeField] private float currentMaxWidth = 5f;
        [SerializeField] private float minWidth = 2f;

        [Space]

        // distances between platforms
        [Range(0f, 1f)]
        [SerializeField] private float doubleYDistanceChance = 0.3f; // chance that platform will be generated in "double" distance
        [SerializeField] private float platformYDistance = 2.5f;
        [SerializeField] private float platformDoubleYDistance = 4f;

        private Transform player;

        private List<GameObject> platforms;
        public ReadOnlyCollection<GameObject> Platforms { get; private set; }

        public Action<Vector2> OnPlatformSpawned;

    #region Generators

        private System.Random platformPrefabChoiceGenerator;
        private System.Random widthGenerator;
        private System.Random xPositionGenerator;
        private System.Random yDistanceChoiceGenerator;
        private float lastPlatformY = 0f;

    #endregion

        private void Start()
        {
            GameManager gm = FindObjectOfType<GameManager>();
            widthGenerator = new System.Random(gm.SeedGenerator.Next());
            xPositionGenerator = new System.Random(gm.SeedGenerator.Next());
            yDistanceChoiceGenerator = new System.Random(gm.SeedGenerator.Next());
            platformPrefabChoiceGenerator = new System.Random(gm.SeedGenerator.Next());

            player = GameObject.FindGameObjectWithTag("Player").transform;
            
            platforms = new List<GameObject>();
            Platforms = new ReadOnlyCollection<GameObject>(platforms);

            if (platformPrefabs.Length == 0)
                Debug.LogError("No platform prefabs set in " + name);
            else
            {
                for (int i = 0; i < initialPlatformCount; i++)
                    GeneratePlatform();
            }
        }

        public bool DeletePlatform(GameObject platform)
        {
            if (platforms.Count == 0) return false;

            return platforms.Remove(platform);
        }

        private void Update() 
        {
            if (platforms.Count == 0) return;

            // check if player distance from highest platform is low enough to generate new platform
            float playerDistanceFromTopPlatform = Mathf.Abs(player.position.y - platforms[platforms.Count - 1].transform.position.y); 
            if (playerDistanceFromTopPlatform <= generationThreshhold)
            {
                GeneratePlatform();
            }
        }

        private void GeneratePlatform()
        {
            if (platformPrefabs.Length == 0) return;
            
            // Create new platform
            int prefabIndex = platformPrefabChoiceGenerator.Next(platformPrefabs.Length);
            GameObject platform = Instantiate(platformPrefabs[prefabIndex], platformParent);
            platforms.Add(platform);

            // Set platform size
            float platformWidth = (float)widthGenerator.NextDouble() * (currentMaxWidth - currentMinWidth) + currentMinWidth;
            var renderer = platform.GetComponent<SpriteRenderer>();
            renderer.size = new Vector2(platformWidth, renderer.size.y);

            // Set collider size
            var coll = platform.GetComponent<EdgeCollider2D>();
            Vector2[] points = new Vector2[2];
            points[0] = new Vector2(-platformWidth / 2, coll.points[0].y);
            points[1] = new Vector2(platformWidth / 2, coll.points[1].y);
            coll.points = points;


            // Set platform position
            // x position
            float xRange = (worldHalfWidth - (platformWidth / 2)); // makes sure platform won't be positioned inside a wall
            float xPos = ((float)xPositionGenerator.NextDouble() * (xRange * 2f)) - xRange;

            // y position
            bool doubleYDistance = yDistanceChoiceGenerator.NextDouble() < doubleYDistanceChance;
            float yDif = doubleYDistance ? platformDoubleYDistance : platformYDistance;
            lastPlatformY += yDif;

            platform.transform.position = new Vector2(xPos, lastPlatformY);

            Vector2 platformSurfacePosition = new Vector2(xPos, lastPlatformY + renderer.bounds.extents.y);
            OnPlatformSpawned?.Invoke(platformSurfacePosition);
        }

        private void OnDrawGizmos()
        {
            // draws helper gizmos for world's extends
            Gizmos.color = Color.magenta;
            Gizmos.DrawLine(new Vector3(-worldHalfWidth, 1, 0), new Vector3(worldHalfWidth, 1, 0));
        }
    }   
}
