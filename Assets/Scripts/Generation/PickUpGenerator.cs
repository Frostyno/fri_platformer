﻿using UnityEngine;
using FRI.Core;
using System;

namespace FRI.Generation
{
    public class PickUpGenerator : MonoBehaviour
    {
        /// <summary>
        /// Used to set up individual probabilities for each type of prefab available individualy.
        /// </summary>
        [System.Serializable]
        class ProbabilityPickUpPair
        {
            public GameObject pickUpPrefab = null;
            [Range(0f, 1f)] public float probability = 0f;
        }

        [SerializeField] private ProbabilityPickUpPair[] pickUpPrefabs = null; // set in editor
        [SerializeField] private Transform pickUpParent = null; // can be null

        [Space]

        [Range(0f, 1f)]
        [SerializeField] private float spawnChance = 0.5f;
        [SerializeField] private float pickUpLevitationHeight = 0.2f;

        private System.Random spawnProbabilityGenerator = null;
        private System.Random pickUpChoiceGenerator = null;

        private void Start()
        {
            if (pickUpPrefabs.Length == 0)
                Debug.LogError("No pick up prefabs present in " + name + '.');

            // make sure that all probabilities added together give 1 (100%)
            float probCounter = 0f;
            foreach (var pair in pickUpPrefabs)
                probCounter += pair.probability;
            if (1f - probCounter >= 0.0001f)
                Debug.LogError("Probabilities must ammount to 1 in " + name + '.');

            var gm = GetComponent<GameManager>();
            spawnProbabilityGenerator = new System.Random(gm.SeedGenerator.Next());
            pickUpChoiceGenerator = new System.Random(gm.SeedGenerator.Next());

            GetComponent<PlatformGenerator>().OnPlatformSpawned += OnPlatformSpawned;
        }

        private void OnPlatformSpawned(Vector2 surfacePosition)
        {
            if (ShouldSpawnPickup())
                SpawnPickup(surfacePosition);
        }

        /// <summary>
        /// Spawn new prefab on targeted platform's surface.
        /// </summary>
        /// <param name="surfacePosition">Platform's surface position.</param>
        private void SpawnPickup(Vector2 surfacePosition)
        {
            int index = GetPrefabIndex(pickUpChoiceGenerator.NextDouble()); // choose which prefab should be spawned

            if (index < 0)
            {
                Debug.LogError("Index error in pick up generator in " + name);
                return;
            }

            GameObject pickUp = Instantiate(pickUpPrefabs[index].pickUpPrefab, pickUpParent); // create new prefab

            Levitator levitator = pickUp.GetComponent<Levitator>();

            // set position
            var initPosition = surfacePosition;
            initPosition.y += pickUp.GetComponent<SpriteRenderer>().bounds.extents.y;
            initPosition.y += levitator == null ? 0f : levitator.Range;
            initPosition.y += pickUpLevitationHeight;
            
            if (levitator != null)
                levitator.SetInitialPosition(initPosition);
            else
                pickUp.transform.position = initPosition;
        }

        /// <summary>
        /// Chooses which prefab should be spawned based on probability provided in parameter.
        /// </summary>
        /// <param name="choiceProbability">Probability generated for this choice.</param>
        /// <returns>Index of pick up prefab.</returns>
        private int GetPrefabIndex(double choiceProbability)
        {
            double temp = 0f;

            for (int i = 0; i < pickUpPrefabs.Length; i++)
            {
                temp += pickUpPrefabs[i].probability;

                if (choiceProbability < temp)
                {
                    return i;
                }
            }

            return -1;
        }

        private bool ShouldSpawnPickup()
        {
            if (pickUpPrefabs.Length == 0) return false;

            double prob = spawnProbabilityGenerator.NextDouble();
            return prob <= spawnChance;
        }
    } 
}
