﻿using FRI.Core;
using UnityEngine;

namespace FRI.UI
{
    public class PlatformerUIStartUp : MonoBehaviour
    {
        [SerializeField] private GameObject controlsGamepad = null;
        [SerializeField] private GameObject controlsKeyboard = null;

        void Start()
        {
            if (GameManager.Instance.Config.gamepadMode)
            {
                controlsKeyboard.SetActive(false);
                controlsGamepad.SetActive(true);
            }
            else
            {
                controlsGamepad.SetActive(false);
                controlsKeyboard.SetActive(true);
            }
        }
    }
}
