﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

/// <summary>
/// Changes button colors in specific events.
/// </summary>
[RequireComponent(typeof(Button))]
public class ButtonTextColorChanger : MonoBehaviour, ISelectHandler, IDeselectHandler
{
    private TextMeshProUGUI text = null;

    [SerializeField] private Color selectedColor = Color.black;
    [SerializeField] private Color deselectedColor = new Color(0.6352941f, 0.9019608f, 0.6941177f, 1f);

    private void Awake()
    {
        text = GetComponentInChildren<TextMeshProUGUI>();
    }

    public void OnSelect(BaseEventData eventData)
    {
        text.color = selectedColor;
    }

    public void OnDeselect(BaseEventData eventData)
    {
        text.color = deselectedColor;
    }

    public void OnDisable()
    {
        text.color = deselectedColor;
    }
}
