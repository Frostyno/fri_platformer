﻿using FRI.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FRI.UI
{
    /// <summary>
    /// Has functionality to change which control scheme is displayed in menu.
    /// </summary>
    public class ControlsMenuManager : MonoBehaviour
    {
        [SerializeField] private Image controlsImage = null;

        [Space]

        [SerializeField] private Sprite controllerControls = null;
        [SerializeField] private Sprite keyboardControls = null;
        
        private void Start()
        {
            if (controlsImage == null) Debug.LogError("Controls image reference missing.");
            controlsImage.sprite = controllerControls;

            if (GameManager.Instance.Config.gamepadMode) DisplayControllerControls();
            else DisplayKeyboardControls();
        }

        public void DisplayControllerControls() => controlsImage.sprite = controllerControls;
        public void DisplayKeyboardControls() => controlsImage.sprite = keyboardControls;
    }
}
