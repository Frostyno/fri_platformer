﻿using UnityEngine;
using TMPro;
using FRI.Core;
using Proyecto26;
using FRI.Models;
using System.Collections;

namespace FRI.UI 
{
    /// <summary>
    /// Score menu manager that handles menu that shows player's score when
    /// he loses the game (on game over).
    /// </summary>
    public class ScoreMenuManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText = null;
        [SerializeField] private TMP_InputField nameInput = null;
        [SerializeField] private GameObject keyboard = null;
        [SerializeField] private GameObject gameOverMenu = null;

        [SerializeField] private string GAME_NAME = "platformer";

        private string serverURL = "http://localhost:5000/api/highscores";

        private string fullURL = "";

        private ScoreHolder scoreHolder = null;
        private UIController uiController = null;

        private bool isNameInputFocused = false;

        private IEnumerator Start()
        {
            if (scoreText == null) Debug.LogError("Score text missing in " + name);
            if (nameInput == null) Debug.LogError("Name input field missing in " + name);
            if (uiController == null) uiController = FindObjectOfType<UIController>();
            scoreHolder = GameManager.Instance.Player.GetComponent<ScoreHolder>();

            serverURL = GameManager.Instance.Config.serverURL;
            fullURL = serverURL + '/' + GAME_NAME;

            scoreText.text = "Skóre: " + scoreHolder.GetTotalScore(); // display total score earned

            yield return null;

            if (!GameManager.Instance.Config.gamepadMode)
            {
                if (keyboard != null) keyboard.SetActive(false);
                if (uiController != null) uiController.FocusInputField(nameInput);
            }
        }

        private void Update()
        {
            if (isNameInputFocused && nameInput.text.Length > 0 && Input.GetKeyDown(KeyCode.Return))
            {
                WriteHighscore();
                gameObject.SetActive(false);
                gameOverMenu.SetActive(true);
                uiController.SetNewMenu(gameOverMenu.transform);
            }

            isNameInputFocused = nameInput.isFocused;
        }

        /// <summary>
        /// Writes new highscore to server's database.
        /// </summary>
        public void WriteHighscore()
        {
            string body = '{' + string.Format("\"name\":\"{0}\",\"score\":{1}", nameInput.text, scoreHolder.GetTotalScore()) + '}';

            RestClient.Post<HighscoreModel>(fullURL, body)
            .Then(res =>
            {
#if UNITY_EDITOR
                Debug.Log(string.Format("ID: {0}, Name: {1}, Score: {2}, Date: {3}", res.id, res.name, res.score, res.date));
#endif
            });
        }

        public void AddNameCharacter(string character)
        {
            if (nameInput.text.Length == nameInput.characterLimit) return;

            nameInput.text += character;
        }

        public void Backspace()
        {
            if (nameInput.text.Length == 0) return;

            nameInput.text = nameInput.text.Substring(0, nameInput.text.Length - 1);
        }
    }
}
