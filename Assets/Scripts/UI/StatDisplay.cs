﻿using FRI.Core;
using FRI.Movement;
using TMPro;
using UnityEngine;

namespace FRI.UI
{
    /// <summary>
    /// Handles displaying player stats during game (score, fuel)
    /// </summary>
    public class StatDisplay : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText = null;
        [SerializeField] private TextMeshProUGUI fuelText = null;
        [SerializeField] private TextMeshProUGUI maxFuelText = null;

        private void Awake() 
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            player.GetComponent<ScoreHolder>().OnTotalScoreChanged += OnTotalScoreChanged;

            if (fuelText == null) return;
            var jetpack = player.GetComponent<Jetpack>();
            maxFuelText.text = Mathf.RoundToInt(jetpack.MaxFuel).ToString();
            fuelText.text = Mathf.RoundToInt(jetpack.CurrentFuel).ToString();
            jetpack.OnFuelChanged += OnFuelChanged;
        }

        private void OnTotalScoreChanged(int currentScore)
        {
            scoreText.text = currentScore.ToString();
        }

        private void OnFuelChanged(float currentFuel)
        {
            fuelText.text = Mathf.FloorToInt(currentFuel).ToString();
        }

        private void OnDestroy()
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            if (player == null) return;
            
            player.GetComponent<ScoreHolder>().OnTotalScoreChanged -= OnTotalScoreChanged;

            if (fuelText == null) return;
            player.GetComponent<Jetpack>().OnFuelChanged -= OnFuelChanged;
        }
    }
}
