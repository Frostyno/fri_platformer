﻿using System;
using System.Collections;
using FRI.Core;
using UnityEngine;

namespace FRI.UI
{
    /// <summary>
    /// Handles displaying of pause menu, game over scree and score menu.
    /// </summary>
    public class PauseMenuManager : MonoBehaviour
    {
        [SerializeField] private UIController uiController = null;

        [Space]

        [SerializeField] private GameObject activeMenu = null;

        [Space]

        [SerializeField] private GameObject pauseMenu = null;
        [SerializeField] private GameObject gameOverMenu = null;
        [SerializeField] private GameObject scoreMenu =  null;

        [Space]

        [SerializeField] private float gameOverMessageLength = 2f;
        [SerializeField] private GameObject gameOverMessage = null;
        [SerializeField] private GameObject controlsTooltip = null;

        private GamePauser gamePauser = null;

        private void Start()
        {
            if (uiController == null) uiController = FindObjectOfType<UIController>();

            GameManager.Instance.OnGameOver += OnGameOver;
            gamePauser = FindObjectOfType<GamePauser>();
            gamePauser.OnPause += OnPause;
        }

        private void OnPause()
        {
            if (GameManager.IsGameOver)
            {
                SetNewActiveMenu(null);
                return;
            }
            SetNewActiveMenu(pauseMenu);
        }

        private void OnGameOver()
        {
            gamePauser.TogglePause();
            if (controlsTooltip != null) controlsTooltip.SetActive(false);
            StartCoroutine(DisplayGameOverMessage());
        }

        private IEnumerator DisplayGameOverMessage()
        {
            if (gameOverMessage != null) gameOverMessage.SetActive(true);

            yield return new WaitForSecondsRealtime(gameOverMessageLength);

            if (gameOverMessage != null) gameOverMessage.SetActive(false);

            SetNewActiveMenu(scoreMenu);
        }

        /// <summary>
        /// Disables currently active menu and sets new active menu based on parameter.
        /// </summary>
        /// <param name="newMenu">Menu to activate.</param>
        private void SetNewActiveMenu(GameObject newMenu)
        {
            if (activeMenu != null) activeMenu.SetActive(false);
            activeMenu = newMenu;
            if (newMenu == null) return;
            newMenu.SetActive(true);
            uiController.SetNewMenu(newMenu.transform);
        }
    }   
}
