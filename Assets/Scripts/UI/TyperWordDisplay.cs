﻿using UnityEngine;
using TMPro;
using FRI.Core;

namespace FRI.UI
{
    /// <summary>
    /// Handles displaying of generated word and showing
    /// correctly and incorrectly written characters.
    /// </summary>
    public class TyperWordDisplay : MonoBehaviour
    {
        private TextMeshPro wordText = null;
        [SerializeField] private float yOffset = 0.4f;

        [SerializeField] private Color32 incorrectColor = new Color32(255, 0, 0, 255);
        [SerializeField] private Color32 correctColor = new Color32(0, 255, 0, 255);
        [SerializeField] private Color32 defaultColor = new Color32(0, 0, 0, 255);

        private void Start()
        {
            var tm = FindObjectOfType<TyperManager>();   
            tm.OnWordGenerated += OnWordGenerated;
            tm.OnCharacterValidation += OnCharacterValidation;
            tm.OnCharacterRemoval += OnCharacterRemoval;

            wordText = GetComponent<TextMeshPro>();
        }

        private void OnWordGenerated(WordGeneratedEventArgs args)
        {
            wordText.text = args.generatedWord; // display generated word
            var pos = args.position;
            pos.y += yOffset;
            wordText.transform.position = pos; // set text object's position over next platform
            wordText.color = defaultColor; // set color to default
        }

        private void OnCharacterValidation(CharacterValidationEventArgs args)
        {
            // change color of character to correct or incorrect color (based on whether it is a correct character)
            ChangeTextCharacterColor(args.index, args.correct ? correctColor : incorrectColor);
        }

        private void OnCharacterRemoval(int charIndex)
        {
            ChangeTextCharacterColor(charIndex, defaultColor);
        }

        /// <summary>
        /// Changes color of a single character in text.
        /// </summary>
        /// <param name="index">Character index.</param>
        /// <param name="color">Target color.</param>
        private void ChangeTextCharacterColor(int index, Color32 color)
        {
            // get character colors and character beginning index (every character has 4 verticles)
            int matIndex = wordText.textInfo.characterInfo[index].materialReferenceIndex;
            Color32[] charColors = wordText.textInfo.meshInfo[matIndex].colors32;
            int vertexIndex = wordText.textInfo.characterInfo[index].vertexIndex;
            
            // change character color to target color for every vertex of the character
            for (int i = 0; i < 4; i++)
                charColors[vertexIndex + i] = color;

            // update text color
            wordText.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);
        }
    }   
}
