﻿using UnityEditor;
using UnityEngine;

namespace FRI.UI
{
    /// <summary>
    /// Shuts down application.
    /// </summary>
    public class GameExiter : MonoBehaviour
    {
        public void ExitGame()
        {
            #if UNITY_EDITOR

            UnityEditor.EditorApplication.isPlaying = false;

            #else

            Application.Quit();

            #endif
        }
    }
}
