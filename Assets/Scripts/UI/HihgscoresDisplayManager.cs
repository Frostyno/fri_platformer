﻿using FRI.Core;
using UnityEngine;
using TMPro;
using System.Text;
using Proyecto26;
using FRI.Models;

namespace FRI.UI
{
    /// <summary>
    /// Handles displaying of highscores in main menu.
    /// </summary>
    public class HihgscoresDisplayManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI currentPageText = null;
        [SerializeField] private TextMeshProUGUI maxPageText = null;
        [SerializeField] private TextMeshProUGUI positionText = null;
        [SerializeField] private TextMeshProUGUI namesText = null;
        [SerializeField] private TextMeshProUGUI scoresText = null;

        [Space]

        [Header("Page settings")]
        [SerializeField] private int pageSize = 10;

        [Space]

        [SerializeField] private string GAME_NAME = "platformer";
        private string serverURL = "http://localhost:5000/api/highscores";

        private int currentPage = 0;
        private int maxPage = 0;

        private StringBuilder positionBuilder = null;
        private StringBuilder namesBuilder = null;
        private StringBuilder scoresBuilder = null;

        private void Start()
        {
            positionBuilder = new StringBuilder();
            namesBuilder = new StringBuilder();
            scoresBuilder = new StringBuilder();

            serverURL = GameManager.Instance.Config.serverURL;

            currentPage = 0; // pages are not used in present version
            // maxPage = (highscoreManager.Highscores.Count - 1) / pageSize;

            currentPageText.text = (currentPage + 1).ToString();
            maxPageText.text = (maxPage + 1).ToString();

            MoveToPage(HighscoresScope.Local);
        }

        private void OnEnable()
        {
            MoveToPage(HighscoresScope.Local);
        }

        // public void NextPage()
        // {
        //     if (currentPage >= maxPage) return;


        //     MoveToPage(currentPage + 1);
        // }

        // public void PreviousPage()
        // {
        //     if (currentPage <= 0) return;

        //     MoveToPage(currentPage - 1); 
        // }

        public void DisplayGlobalHighscores()
        {
            MoveToPage(HighscoresScope.Global);
        }

        public void DisplayLocalHighscores()
        {
            MoveToPage(HighscoresScope.Local);
        }

        private void MoveToPage(HighscoresScope scope)
        {
            string url = serverURL + '/' + GAME_NAME;
            url += scope == HighscoresScope.Global ? "/global" : "/local";
            url += '/' + pageSize.ToString() + '/' + 1; // 1 is pageNumber
            
            // load highscores from given scope from server's database
            RestClient.GetArray<HighscoreModel>(url).Then(
                response =>
                {
                    positionBuilder.Clear();
                    namesBuilder.Clear();
                    scoresBuilder.Clear();

                    int index = 1;
                    foreach (var highscore in response) // build highscore strings
                    {
                        positionBuilder.Append(index.ToString());
                        positionBuilder.AppendLine(".");
                        namesBuilder.AppendLine(highscore.name);
                        scoresBuilder.AppendLine(highscore.score.ToString());
                        index++;
                    }

                    // display highscores
                    positionText.text = positionBuilder.ToString();
                    namesText.text = namesBuilder.ToString();
                    scoresText.text = scoresBuilder.ToString();
                }
            );
        }
    }   
}
