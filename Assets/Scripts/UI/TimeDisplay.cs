﻿using TMPro;
using UnityEngine;
using FRI.Core;
using System;
using System.Text;

/// <summary>
/// Displays remaining time for player during gameplay.
/// </summary>
public class TimeDisplay : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI timeText = null;
    
    private StringBuilder sb = new StringBuilder();

    private void Awake()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<CountdownTimeHolder>().OnCountdownTimeChanged += OnCountdownTimeChanged;

        if (timeText == null)
        {
            Debug.LogError("Time text not set in " + name);
        }
    }

    private void OnCountdownTimeChanged(float time)
    {
        if (timeText == null) return;
        timeText.text = FormatTime(time);
    }

    private string FormatTime(float time)
    {
        if (time <= 0) return "00:00";

        if (sb.Length != 0) sb.Clear();

        sb.Append(Mathf.FloorToInt(time / 60).ToString("00")); // minutes
        sb.Append(':');
        sb.Append(Mathf.CeilToInt(time % 60).ToString("00")); // seconds

        return sb.ToString();
    }
}
