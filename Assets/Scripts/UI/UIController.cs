﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace FRI.UI
{
    /// <summary>
    /// Handles navigating UI using controller.
    /// </summary>
    public class UIController : MonoBehaviour
    {
        [SerializeField] private EventSystem eventSystem = null;
        [SerializeField] private GameObject currentlySelected = null;

        private void Awake()
        {
            if (eventSystem == null) eventSystem = FindObjectOfType<EventSystem>();
        }

        private void Update()
        {
            if (currentlySelected == null) return;
            if (!currentlySelected.activeInHierarchy) return;
            if (currentlySelected == eventSystem.currentSelectedGameObject) return;

            // tell event system to select object that should be selected (prevent mouse click deselect issue)
            if (eventSystem.currentSelectedGameObject == null)
            {
                eventSystem.SetSelectedGameObject(currentlySelected);
                return;
            }

            currentlySelected = eventSystem.currentSelectedGameObject;
        }

        public void SetNewMenu(Transform menu)
        {
            eventSystem.SetSelectedGameObject(menu.GetComponentInChildren<Button>().gameObject);
            currentlySelected = eventSystem.currentSelectedGameObject;
        }

        public void FocusInputField(TMPro.TMP_InputField inputField)
        {
            eventSystem.SetSelectedGameObject(inputField.gameObject);
            currentlySelected = eventSystem.currentSelectedGameObject;
        }
    }
}
