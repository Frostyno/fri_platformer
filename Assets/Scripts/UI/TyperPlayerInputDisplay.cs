﻿using TMPro;
using FRI.Core;
using UnityEngine;

namespace FRI.UI
{
    /// <summary>
    /// Deprecated. Currently used is TyperWordDisplay.
    /// </summary>
    public class TyperPlayerInputDisplay : MonoBehaviour
    {
        private TextMeshPro inputText = null;
        [SerializeField] private float yOffset = -1.4f;

        private void Awake()
        {
            var tm = FindObjectOfType<TyperManager>();
            tm.OnWordGenerated += OnWordGenerated;
            tm.OnCharacterValidation += OnCharacterValidation;
            tm.OnCharacterRemoval += OnCharacterRemoval;

            inputText = GetComponent<TextMeshPro>();
        }

        private void OnWordGenerated(WordGeneratedEventArgs args)
        {
            inputText.text = null;

            var pos = args.position;
            pos.y += yOffset;
            inputText.transform.position = pos;
        }

        private void OnCharacterRemoval(int index)
        {
            if (inputText.text == null) return;

            inputText.text = inputText.text.Substring(0, inputText.text.Length - 1);
        }

        private void OnCharacterValidation(CharacterValidationEventArgs args)
        {
            if (inputText.text != null) inputText.text = inputText.text + args.input;
            else inputText.text = args.input.ToString();
        }
    }   
}
