﻿using System;
using System.Collections;
using UnityEngine;

namespace FRI.Movement
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class Mover : MonoBehaviour
    {

        #region Attributes
        
        [SerializeField] private Transform bottomCenter = null; // used for ground detection when jumping
        [SerializeField] private float horizontalSpeed = 10f;
        [SerializeField] private float jumpForce = 10f; // strength of jump
        [SerializeField] private float maxFallSpeed = 5f;
        [Space]
        [SerializeField] private ParticleSystem jumpParticleSystem = null;
        [SerializeField] private int particlesToEmit = 10; // number of particles that should be emitted when jumping

        private Animator animator = null;
        private Rigidbody2D rb;
        private Vector2 c_groundCheckSize = Vector2.zero;
        private const float c_groundCheckDepth = 0.2f; // how deep the ground check should be (search for platform)

        private float _horizontalInput = 0f;
        public float HorizontalInput
        {
            get => _horizontalInput;
            set 
            {
                // _horizontalInput = Mathf.Clamp(value, -1f, 1f);
                _horizontalInput = value == 0 ? 0 : Mathf.Sign(value) * 1; // only allow values of 1, -1 and 0
            }
        }

        private bool facingRight = false;

        private bool _jumpInput = false;
        public bool JumpInput { get => _jumpInput; set => _jumpInput = value; }

        private bool grounded = true;
        private bool jumpedThisFrame = false;
        private bool canJump = true;
        private bool canDoubleJump = true;

        private float horizontalVelocityModifier = 0f;

        #endregion

        private void Awake()
        {
            rb = GetComponent<Rigidbody2D>();
            animator = GetComponent<Animator>();
            var coll = GetComponent<Collider2D>();
            c_groundCheckSize = new Vector2(coll.bounds.size.x, c_groundCheckDepth);
        }

        private void FixedUpdate()
        {
            UpdateVelocity();
            CheckRotation();

            if (JumpInput)
            {
                JumpInput = false;
                Jump();
            }

            CheckGroundCollision();
            UpdateAnimator();
        }

        private void UpdateVelocity()
        {
            Vector2 velocity = rb.velocity;
            if (horizontalVelocityModifier == 0f) // if there are no movement modifiers, move based on input
                velocity.x = HorizontalInput * horizontalSpeed;
            else
                velocity.x = horizontalVelocityModifier; // if there are movement modifiers present apply them

            // limit falling speed
            velocity.y = Mathf.Max(-maxFallSpeed, velocity.y);

            rb.velocity = velocity;
        }

        /// <summary>
        /// If entity isn't facing the correct way, flip it around
        /// </summary>
        private void CheckRotation()
        {
            if (HorizontalInput >= 0 && facingRight) return;
            if (HorizontalInput <= 0 && !facingRight) return;

            transform.Rotate(0f, 180f, 0f);
            facingRight = !facingRight;
        }

        private void CheckGroundCollision()
        {
            if (bottomCenter == null) return;
            if (jumpedThisFrame) // don't check collision when entity just jumped
            {
                jumpedThisFrame = false;
                return;
            }

            if (rb.velocity.y > 0) return; // dont check collision if entity isn't falling

            // TODO: consider storing layer mask in attribute
            Vector2 checkPosition = bottomCenter.position;
            checkPosition.y -= (c_groundCheckSize.y / 2);
            Collider2D ground = Physics2D.OverlapBox(checkPosition, c_groundCheckSize, 0, LayerMask.GetMask("Ground")); // check for collision
            
            // if isn't grounded
            if (ground == null)
            {
                grounded = false;
                return;
            }
            
            grounded = true;
            canDoubleJump = true;
        }

        private void Jump()
        {
            if (!canJump) return;

            bool jump = false;

            if (grounded) // jump if standing on the ground
            {
                grounded = false;
                jumpedThisFrame = true;
                jump = true;
            }
            else if (canDoubleJump) // if is in air and hasn't jumped yet, jump
            {
                canDoubleJump = false;
                jump = true;
            }

            if (jump) // if some of the jump conditions are met, do the jumping
            {
                rb.velocity = new Vector2(rb.velocity.x, 0);
                rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
                // if (animator != null) animator.SetTrigger("jump");
                if (jumpParticleSystem != null) jumpParticleSystem.Emit(particlesToEmit);
            }
        }

        /// <summary>
        /// Updates entity's animator controller.
        /// </summary>
        private void UpdateAnimator()
        {
            if (animator == null) return;

            animator.SetFloat("horizontalVelocity", Mathf.Abs(rb.velocity.x));
            animator.SetFloat("verticalVelocity", rb.velocity.y);
            animator.SetBool("grounded", grounded);
        }

        /// <summary>
        /// Starts movement modifier coroutine (sets movement modifier for specified time).
        /// </summary>
        /// <param name="bonusVelocity">Movement modifier.</param>
        /// <param name="length">Modifier length.</param>
        public void ApplyBonusVelocityFor(float bonusVelocity, float length)
        {
            StartCoroutine(AddHorizontalVelocityModifierFor(bonusVelocity, length));
        }

        private IEnumerator AddHorizontalVelocityModifierFor(float modifier, float length)
        {
            horizontalVelocityModifier += modifier;
            yield return new WaitForSeconds(length);
            horizontalVelocityModifier -= modifier;
        }

        /// <summary>
        /// Starts coroutine that disables jumping for given time.
        /// </summary>
        /// <param name="length">Jumping disable length.</param>
        public void DisableJumpingFor(float length)
        {
            StartCoroutine(DisableJumping(length));
        }

        private IEnumerator DisableJumping(float length)
        {
            canJump = false;
            yield return new WaitForSeconds(length);
            canJump = true;
        }

        /// <summary>
        /// Draws helper gizmos to visualize where entity checks for ground.
        /// Works in editor.
        /// </summary>
        private void OnDrawGizmosSelected() {
            if (bottomCenter == null) return;

            Gizmos.color = Color.black;
            Vector2 checkPosition = bottomCenter.position;
            checkPosition.y -= (c_groundCheckSize.y / 2);
            var coll = GetComponent<Collider2D>();
            Gizmos.DrawWireCube(checkPosition, c_groundCheckSize);
        }
    }
}
