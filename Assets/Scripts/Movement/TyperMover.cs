﻿using System.Collections;
using System.Collections.Generic;
using FRI.Core;
using UnityEngine;

namespace FRI.Movement
{
    public class TyperMover : MonoBehaviour
    {
        [SerializeField] private float yOffset = 0.5f;
        [Range(0f, 1f)]
        [SerializeField] private float speed = 0.2f;

        private bool targetActive = false;
        private Vector3 targetPosition = Vector2.zero;

        private Animator animator = null;

        private void Awake()
        {
            FindObjectOfType<TyperManager>().OnWordWritten += OnWordWritten;
            animator = GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>();
        }

        public void OnWordWritten(WordWrittenEventArgs args)
        {
            MoveToPosition(args.position);
        }

        public void MoveToPosition(Vector2 position)
        {
            Vector3 scale = transform.localScale;
            if (position.x > transform.position.x) // if needed, change facing direction
            {
                scale.x = -1;
            }
            else
            {
                scale.x = 1;
            }

            transform.localScale = scale;

            // set where the player should be moved.
            targetPosition = new Vector3(position.x, position.y + yOffset);
            targetActive = true;
            animator.SetTrigger("Jumped");
        }

        private void FixedUpdate()
        {
            UpdatePosition();
        }

        /// <summary>
        /// If the player hasn't reached targetPosition yet, moves him in that direction.
        /// </summary>
        private void UpdatePosition()
        {
            if (!targetActive) return;

            transform.position = Vector2.MoveTowards(transform.position, targetPosition, speed);

            if (transform.position == targetPosition)
            {
                targetActive = false;
                animator.SetTrigger("Landed");
            }
        }
    }
}
