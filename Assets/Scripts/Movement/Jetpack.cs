﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRI.Movement
{
    /// <summary>
    /// Handles jetpack logic.
    /// </summary>
    [RequireComponent(typeof(Rigidbody2D))]
    public class Jetpack : MonoBehaviour
    {
        #region Attributes

        [SerializeField] private float jetpackForce = 5f;

        [SerializeField] private float maxFuel = 100f;
        [SerializeField] private float fuelPerSecond = 3f;
        [SerializeField] private float maxUpJetpackSpeed = 10f; // specifies maximum speed of jetpack
        [Space]
        [SerializeField] private ParticleSystem jetpackThruster = null;
        [SerializeField] private int particlesToEmit = 3;
        private float currentFuel;

        private Rigidbody2D rb = null;

        private bool _active = false;
        public bool Active
        {
            get => _active;
            set
            {
                if (_active != value)
                    _active = value;
            }
        }

        public float MaxFuel { get => maxFuel; }
        public float CurrentFuel { get => currentFuel; }

        public Action<float> OnFuelChanged = null;

        #endregion

        private void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            currentFuel = maxFuel;
            OnFuelChanged?.Invoke(currentFuel);

            if (jetpackThruster == null) Debug.LogError("Jetpack particle system missing in " + name);
        }

        private void FixedUpdate()
        {
            if (!Active) return;
            if (currentFuel <= 0f) return;

            ConsumeFuel();

            if (rb.velocity.y < maxUpJetpackSpeed)
                ApplyThrust();

            if (jetpackThruster != null)
                jetpackThruster.Emit(particlesToEmit);
        }

        private void ConsumeFuel()
        {
            if (currentFuel == 0f) return;

            float fuelToConsume = fuelPerSecond * Time.fixedDeltaTime;
            currentFuel = Mathf.Max(0f, currentFuel - fuelToConsume);
            OnFuelChanged?.Invoke(currentFuel);
        }

        private void ApplyThrust()
        {
            rb.AddForce(Vector2.up * jetpackForce * Time.fixedDeltaTime);
        }

        public void GainFuel(float ammount)
        {
            if (currentFuel == maxFuel) return;

            currentFuel = Mathf.Min(maxFuel, currentFuel + ammount);
            OnFuelChanged?.Invoke(currentFuel);
        }
    }   
}
