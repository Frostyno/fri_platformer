﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FRI.Models
{
    [System.Serializable]
    public partial class User
    {
        public string name;
        public string password;
        public string token;
    }
}
