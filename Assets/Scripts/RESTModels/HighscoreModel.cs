﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRI.Models
{
    [System.Serializable]
    public partial class HighscoreModel
    {
        public long id;
        public string name;
        public long score;
        public string date;
    }
}
