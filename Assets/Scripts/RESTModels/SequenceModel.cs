﻿namespace FRI.Models
{
    [System.Serializable]
    public partial class SequenceModel
    {
        public long id;
        public string word;
        public string inputsequence;
        public decimal timetaken;
    }
}