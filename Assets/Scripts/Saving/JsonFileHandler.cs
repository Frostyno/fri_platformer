﻿using System.IO;
using Newtonsoft.Json;
using UnityEngine;

namespace FRI.Saving
{
    /// <summary>
    /// Utility class used for saving objects to json and then loading them.
    /// </summary>
    public class JsonFileHandler
    {
        public static void Serialize(object objectToSerialize, string path)
        {
            using (StreamWriter file = File.CreateText(path))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Formatting = Formatting.Indented;
                serializer.Serialize(file, objectToSerialize);
            }
        }

        public static T Deserialize<T>(string path) where T : class
        {
            if (!File.Exists(path)) return null;
            using (JsonReader file = new JsonTextReader(File.OpenText(path)))
            {
                JsonSerializer serializer = new JsonSerializer();
                return serializer.Deserialize<T>(file);
            }
        }
    }
}
