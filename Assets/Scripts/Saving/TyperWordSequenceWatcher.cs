﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using FRI.Core;
using UnityEngine;

namespace FRI.Saving
{
    public class TyperWordSequenceWatcher : MonoBehaviour
    {
        private WordSequenceManager wsm = null;

        private StringBuilder sb = null;
        private string currentWord = null;

        private void Start()
        {
            wsm = FindObjectOfType<WordSequenceManager>();
            if (wsm == null) 
            {
                Debug.LogError("WordSequenceManager is missing from scene.");
                return;
            }

            sb = new StringBuilder();

            var tm = GetComponent<TyperManager>();
            tm.OnWordWritten += OnWordWritten;
            tm.OnCharacterRemoval += OnCharacterRemoval;
            tm.OnCharacterValidation += OnCharacterValidation;
            tm.OnWordGenerated += OnWordGenerated;
        }

        private void OnWordGenerated(WordGeneratedEventArgs args)
        {
            currentWord = args.generatedWord;
        }

        private void OnWordWritten(WordWrittenEventArgs args)
        {
            wsm.AddNewSequence(currentWord, sb.ToString(), args.timeTaken);
            sb.Clear();
            currentWord = null;
        }

        private void OnCharacterRemoval(int index)
        {
            sb.Append("\\b");
        }

        private void OnCharacterValidation(CharacterValidationEventArgs args)
        {
            sb.Append(args.input);
        }
    }
}
