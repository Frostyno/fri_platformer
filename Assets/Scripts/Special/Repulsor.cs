﻿using FRI.Movement;
using UnityEngine;

namespace FRI.Special
{
    [RequireComponent(typeof(Collider2D))]
    public class Repulsor : MonoBehaviour
    {
        [SerializeField] private float horizontalRepulseVelocity = 5f;
        [SerializeField] private float verticalRepulseForce = 10f;
        [SerializeField] private float effectLength = 0.5f;

        /// <summary>
        /// When this GameObject collides with player it repulses him away based on attributes.
        /// </summary>
        private void OnCollisionEnter2D(Collision2D other) 
        {
            if (other.gameObject.tag != "Player") return;
            
            Mover playerMover = other.gameObject.GetComponent<Mover>();
            Rigidbody2D playerRB = other.gameObject.GetComponent<Rigidbody2D>();
            
            // determine whether the player is to the right or to the left
            int direction = transform.position.x < other.transform.position.x ? 1 : -1;

            // add movement modifier for player
            playerMover.ApplyBonusVelocityFor(horizontalRepulseVelocity * direction, effectLength);
            // disable player's jumping
            playerMover.DisableJumpingFor(effectLength);
            playerRB.AddForce(new Vector2(0, verticalRepulseForce), ForceMode2D.Impulse);
        }
    }   
}
